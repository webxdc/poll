# Poll

A Webxdc with a simple poll -
one question,
up to 5 answers.

![Screenshot](assets/screenshot1.png) ![Screenshot](assets/screenshot2.png) ![Screenshot](assets/screenshot3.png)

[Download .xdc from Release Assets](https://codeberg.org/webxdc/poll/releases), attach to a Delta Chat group, create your poll and get votes from all group members!

[Online-Demo](https://webxdc.codeberg.page/poll/@master/)


## Building

to create a `.xdc` file that can be attached to a Delta Chat group, execute:

```sh
./create-xdc.sh
```
